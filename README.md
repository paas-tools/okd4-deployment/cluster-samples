# cluster-samples

This repository contains a Helm chart that deploys OpenShift `ImageStreams` and `Templates` for an OKD4 cluster.

Starting with OKD 4.13 the [Cluster Samples Operator is being "downsized"](https://docs.openshift.com/container-platform/4.15/openshift_images/configuring-samples-operator.html#samples-operator-configuration_configuring-samples-operator).
It provides a limited set of functionality and in the future will provide even less.
Most importantly for us, it does not apply updates to existing ImageStreamTags (only *new* tags are added).

To mitigate this, we started taking control of the ImageStreams and Templates deployed by the Cluster Samples Operator, this means we are now managing the manifests of these resources.
They are stored in this repository and are based on the upstream source: <https://github.com/openshift/cluster-samples-operator/tree/master/assets/operator/okd-x86_64>
In practice this means that the updates and the set of available of tags is no longer tied to the OKD version we are running.

To provide regular updates, a CronJob was added that iterates over all ImageStreamTags annotated with `okd.cern.ch/scheduled-import=true`.
We use a CronJob because it allows us to finely control the frequency of these updates.
If we used OKD's "scheduled image import" feature, some BuildConfigs would be triggered several times per day!

For more background information, refer to the issues and merge requests related to <https://gitlab.cern.ch/groups/webservices/-/epics/22>.

## Updates to existing tags

The aforementioned CronJob called `updated-managed-s2i-imagestreams` regularly (~once a month) triggers an import of ImageStreamTags annotated with `okd.cern.ch/scheduled-import=true`.
To run the job manually (e.g. after an OKD upgrade), use the following command:

```sh
oc -n openshift-cern-paas debug cronjob/update-managed-s2i-imagestreams -- /scripts/import-annotated-istags.sh
```

## Adding new tags

> New tags must be placed in alphabetical order based on the `name` field to prevent ArgoCD application `cluster-samples` in `okd4-install` repository being constantly in `OutOfSync` state. See [related MR](https://gitlab.cern.ch/paas-tools/okd4-deployment/cluster-samples/-/merge_requests/5).

From time to time we may want to add more (newer) tags to the ImageStreams, e.g. `openshift/python:3.12-ubi9`.
To do this, we need to compare the upstream sourced found [here](https://github.com/openshift/cluster-samples-operator/tree/master/assets/operator/okd-x86_64) to our local manifests.
To get a quick overview of which tags have recently been added, we can compare the latest "release" branches: <https://github.com/openshift/cluster-samples-operator/compare/release-4.14...release-4.15>

Note that a conversion from JSON to YAML is required, e.g.:

```sh
curl -sSL https://github.com/openshift/cluster-samples-operator/raw/master/assets/operator/okd-x86_64/python/imagestreams/python-centos.json | yq -y .
```

Then the relevant YAML section can be added to our local manifests - make sure that the elements of the `tags` array are sorted according to the `name` (otherwise ArgoCD will continuously reconcile the manifests):

```yaml
    - name: 3.11-ubi9
      annotations:
        description: Build and run Python 3.11 applications on UBI 9. For more information
          about using this builder image, including OpenShift considerations, see
          https://github.com/sclorg/s2i-python-container/blob/master/3.9/README.md.
        iconClass: icon-python
        openshift.io/display-name: Python 3.11 (UBI 9)
        openshift.io/provider-display-name: Red Hat, Inc.
        sampleRepo: https://github.com/sclorg/django-ex.git
        supports: python:3.11,python
        tags: builder,python
        version: '3.11'
      from:
        kind: DockerImage
        name: registry.access.redhat.com/ubi9/python-311:latest
      importPolicy: {}
      referencePolicy:
        type: Local
```

## Removing tags

Sometimes we may need to remove ImageStreamTags as well, for example because the upstream image is no longer available.
Removing a single tag is as simple as
However we can only do this when no user project relies on this image anymore with their BuildConfig/DeploymentConfig/...
The included script can be used to check if an ImageStreamTag is still used, e.g.:

```sh
./is-imagestreamtag-used.sh python:latest
+ deployments:
+ buildconfigs:
["anomaly-detection-mki","dash"]
["apple-signing","apple-signing"]
["chatlas","chatlas-flask"]
["cms-project-aachen3a-db","aachen3a-db"]
["ebis","ebisim-dash"]
["larmon","larmon-test"]
["pigbt","lpgbt-test"]
["pigbt","pigbt"]
["test-app","csc-rack-temperature-monitor-git"]
["test-ot-hybrids-vi","ot-hybrids-vi-webapp"]
+ deploymentconfigs:
["epdt-rpc-dcs","epdt-rpc-dcs-component-epdt-rpc-dcs-component"]
+ pods:
["epdt-rpc-dcs","epdt-rpc-dcs-component-epdt-rpc-dcs-component-3-c9n5c"]
```
