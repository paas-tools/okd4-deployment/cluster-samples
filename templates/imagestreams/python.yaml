---
apiVersion: image.openshift.io/v1
kind: ImageStream
metadata:
  name: python
  namespace: openshift
  annotations:
    # Name used in the UI
    openshift.io/display-name: Python
    # ArgoCD should never delete this resource
    argocd.argoproj.io/sync-options: Delete=false,Prune=false
spec:
  lookupPolicy:
    local: false
  tags:
    - annotations:
        description:
          Build and run Python 2.7 applications on CentOS 7. For more information
          about using this builder image, including OpenShift considerations, see https://github.com/sclorg/s2i-python-container/blob/master/2.7/README.md.
        iconClass: icon-python
        openshift.io/display-name: Python 2.7
        openshift.io/provider-display-name: Red Hat, Inc.
        sampleRepo: https://github.com/sclorg/django-ex.git
        supports: python:2.7,python
        tags: builder,python,hidden
        version: "2.7"
        # this image is no longer available upstream
        okd.cern.ch/scheduled-import: "false"
        # the annotation may be used in the future to notify project owners about this
        okd.cern.ch/deprecated: "true"
      from:
        kind: DockerImage
        name: quay.io/centos7/python-27-centos7:latest
      importPolicy:
        importMode: Legacy
      name: "2.7"
      referencePolicy:
        type: Local
    - annotations:
        description:
          Build and run Python 2.7 applications on UBI 8. For more information
          about using this builder image, including OpenShift considerations, see https://github.com/sclorg/s2i-python-container/blob/master/2.7/README.md.
        iconClass: icon-python
        openshift.io/display-name: Python 2.7 (UBI 8)
        openshift.io/provider-display-name: Red Hat, Inc.
        sampleRepo: https://github.com/sclorg/django-ex.git
        supports: python:2.7,python
        tags: builder,python,hidden
        version: "2.7"
        okd.cern.ch/scheduled-import: "enabled"
      from:
        kind: DockerImage
        name: registry.access.redhat.com/ubi8/python-27:latest
      importPolicy:
        importMode: Legacy
      name: 2.7-ubi8
      referencePolicy:
        type: Local
    - annotations:
        description:
          Build and run Python 3.11 applications on UBI 9. For more information
          about using this builder image, including OpenShift considerations, see https://github.com/sclorg/s2i-python-container/blob/master/3.11/README.md.
        iconClass: icon-python
        openshift.io/display-name: Python 3.11 (UBI 9)
        openshift.io/provider-display-name: Red Hat, Inc.
        sampleRepo: https://github.com/sclorg/django-ex.git
        supports: python:3.11,python
        tags: builder,python
        version: "3.11"
        okd.cern.ch/scheduled-import: "enabled"
      from:
        kind: DockerImage
        name: registry.access.redhat.com/ubi9/python-311:latest
      importPolicy:
        importMode: Legacy
      name: 3.11-ubi9
      referencePolicy:
        type: Local
    - annotations:
        description:
          Build and run Python 3.6 applications on UBI 8. For more information
          about using this builder image, including OpenShift considerations, see https://github.com/sclorg/s2i-python-container/blob/master/3.6/README.md.
        iconClass: icon-python
        openshift.io/display-name: Python 3.6 (UBI 8)
        openshift.io/provider-display-name: Red Hat, Inc.
        sampleRepo: https://github.com/sclorg/django-ex.git
        supports: python:3.6,python
        tags: builder,python
        version: "3.6"
        okd.cern.ch/scheduled-import: "enabled"
      from:
        kind: DockerImage
        name: registry.access.redhat.com/ubi8/python-36:latest
      importPolicy:
        importMode: Legacy
      name: 3.6-ubi8
      referencePolicy:
        type: Local
    - annotations:
        description:
          Build and run Python 3.8 applications on UBI 8. For more information
          about using this builder image, including OpenShift considerations, see https://github.com/sclorg/s2i-python-container/blob/master/3.8/README.md.
        iconClass: icon-python
        openshift.io/display-name: Python 3.8 (UBI 8)
        openshift.io/provider-display-name: Red Hat, Inc.
        sampleRepo: https://github.com/sclorg/django-ex.git
        supports: python:3.8,python
        tags: builder,python
        version: "3.8"
        okd.cern.ch/scheduled-import: "enabled"
      from:
        kind: DockerImage
        name: registry.access.redhat.com/ubi8/python-38:latest
      importPolicy:
        importMode: Legacy
      name: 3.8-ubi8
      referencePolicy:
        type: Local
    - annotations:
        description:
          Build and run Python 3.9 applications on UBI 8. For more information
          about using this builder image, including OpenShift considerations, see https://github.com/sclorg/s2i-python-container/blob/master/3.9/README.md.
        iconClass: icon-python
        openshift.io/display-name: Python 3.9 (UBI 8)
        openshift.io/provider-display-name: Red Hat, Inc.
        sampleRepo: https://github.com/sclorg/django-ex.git
        supports: python:3.9,python
        tags: builder,python
        version: "3.9"
        okd.cern.ch/scheduled-import: "enabled"
      from:
        kind: DockerImage
        name: registry.access.redhat.com/ubi8/python-39:latest
      importPolicy:
        importMode: Legacy
      name: 3.9-ubi8
      referencePolicy:
        type: Local
    - annotations:
        description:
          Build and run Python 3.9 applications on UBI 9. For more information
          about using this builder image, including OpenShift considerations, see https://github.com/sclorg/s2i-python-container/blob/master/3.9/README.md.
        iconClass: icon-python
        openshift.io/display-name: Python 3.9 (UBI 9)
        openshift.io/provider-display-name: Red Hat, Inc.
        sampleRepo: https://github.com/sclorg/django-ex.git
        supports: python:3.9,python
        tags: builder,python
        version: "3.9"
        okd.cern.ch/scheduled-import: "enabled"
      from:
        kind: DockerImage
        name: registry.access.redhat.com/ubi9/python-39:latest
      importPolicy:
        importMode: Legacy
      name: 3.9-ubi9
      referencePolicy:
        type: Local
    - annotations:
        description: |-
          Build and run Python applications on UBI. For more information about using this builder image, including OpenShift considerations, see https://github.com/sclorg/s2i-python-container/blob/master/3.9/README.md.

          WARNING: By selecting this tag, your application will automatically update to use the latest version of Python available on OpenShift, including major version updates.
        iconClass: icon-python
        openshift.io/display-name: Python (Latest)
        openshift.io/provider-display-name: Red Hat, Inc.
        sampleRepo: https://github.com/sclorg/django-ex.git
        supports: python
        tags: builder,python
        okd.cern.ch/scheduled-import: "enabled"
      from:
        kind: ImageStreamTag
        name: 3.11-ubi9
      importPolicy:
        importMode: Legacy
      name: latest
      referencePolicy:
        type: Local
