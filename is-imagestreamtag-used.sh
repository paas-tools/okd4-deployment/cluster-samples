#!/bin/bash

ISTAG="$1"

# keep a local cache of these resources to speed up subsequent invocations
[ ! -f /tmp/pods.json ] && oc get pods -A -o json > /tmp/pods.json
[ ! -f /tmp/buildconfigs.json ] && oc get bc -A -o json > /tmp/buildconfigs.json
[ ! -f /tmp/deploymentconfigs.json ] && oc get dc -A -o json > /tmp/deploymentconfigs.json
[ ! -f /tmp/deployments.json ] && oc get deploy -A -o json > /tmp/deployments.json

echo "+ deployments:"
jq -c -r '.items[] | select(.metadata.annotations."image.openshift.io/triggers" // "" | test("openshift\/'${ISTAG}'")) | [.metadata.namespace, .metadata.name]' /tmp/deployments.json

echo "+ buildconfigs:"
jq -c -r '.items[] | select(.spec.strategy.sourceStrategy.from.name?=="'${ISTAG}'") | [.metadata.namespace, .metadata.name]' /tmp/buildconfigs.json

echo "+ deploymentconfigs:"
jq -c -r '.items[] | select(.spec.triggers[].imageChangeParams.from.name?=="'${ISTAG}'") | [.metadata.namespace, .metadata.name]' /tmp/deploymentconfigs.json

echo "+ pods:"
REF=$(oc get istag -n openshift "${ISTAG}" -o jsonpath='{.image.dockerImageReference}')
jq -c -r '.items[] | select(.spec.containers[].image == "'${REF}'") | [.metadata.namespace, .metadata.name]' /tmp/pods.json
